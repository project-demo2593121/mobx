import { createContext, useContext } from 'react'
import { action, makeAutoObservable, makeObservable, observable, autorun } from 'mobx'

class Count {
	count
	number

	constructor() {
		// makeObservable(this, {
		// 	count: observable,
		// 	increase: action,
		// })
		makeAutoObservable(this)

		//observable
		this.count = 0
		this.number = 0

		//autorun
		// break khi gọi đến func trả về dispose
		autorun((reaction) => {
			console.log(this)
			if (this.total > 500) {
				reaction.dispose()
			}
		})
	}

	//computed
	get total() {
		return (this.number = this.count * 5)
	}

	//action
	increase = () => {
		this.count += 10
	}
}

export const rootStore = {
	countStore: new Count(),
}

export type TRootStore = typeof rootStore
const RootStoreContext = createContext<null | TRootStore>(null)

export const Provider = RootStoreContext.Provider

export function useStore() {
	const store = useContext(RootStoreContext)
	if (store === null) {
		throw new Error('Store cannot be null, please add a context provider')
	}
	return store
}
