import React from 'react'
import { observer } from 'mobx-react'

import { useStore } from '../store'

const Counter = observer(() => {
	const { countStore } = useStore()

	const { count, increase, total } = countStore

	return (
		<div>
			<p>Counter: {count}</p>

			<p>Counter * 5: {total}</p>

			<button onClick={increase}>Increase</button>
		</div>
	)
})

export default Counter
