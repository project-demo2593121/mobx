import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { Provider, rootStore } from './store.ts'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
	// <React.StrictMode>
	<Provider value={rootStore}>
		<App />
	</Provider>
	// </React.StrictMode>
)
